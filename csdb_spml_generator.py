import numpy as np
import tempfile
'#!/usr/bin/env python'
'# -*- encoding: utf-8 -*-'


'#Copyright Ayub Kibirio         '
'#Kenya 2019/08/01                '
'#Start of the Handler Project'
'#!/usr/bin/env python'


Line = "================================================================================\n"
print(Line)
print(">>> AYUB : SPRINTECHLAB | CSDB SPML GENERATOR")
print(Line)

'#function to generate SPML'
'#get data from an input file:'
'#D:\2018-2019\SDM Planning\LTE SHARING\Work Area\ltecards.txt'


def generate_spml():
    input_file = input("Drag $ Drop a file [IMSI,MSISDN,A4KI,ICCID]: ")
    '#define imsi and msisdn variables as arrays:'
    imsi = []
    msisdn = []
    with open(input_file) as file:
        content = file.read()
        my_data = content.splitlines()
        for item in my_data:
            nam_item = item.split(',')
            np_nam_item = np.array(nam_item)
            imsi.append(np_nam_item[0])
            msisdn.append(np_nam_item[1])
            '#convert imsi and msisdn as numpy array'
    data = [imsi,msisdn]

    head = "<spml:batchRequest \n language=\"en_us\" \n execution=\"synchronous\" \n " \
           "processing=\"parallel\" \n xmlns:spml=\"urn:siemens:names:prov:gw:SPML:2:0\" \n " \
           "xmlns:subscriber=\"urn:siemens:names:prov:gw:HLR_SUBSCRIBER:4:5\"  \n " \
           "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"  \n onError=\"resume\"> \n " \
           "<version xmlns:subscriber=\"urn:siemens:names:prov:gw:HLR_SUBSCRIBER:4:5\">HLR_SUBSCRIBER_v45</version>"
    foot = "</spml:batchRequest>"

    spml_list = []
    spml_list.append(head)

    for i in range(len(data[0])):
        rec_count = len(data[0])
        spml_cnt = "<request xsi:type=\"spml:ModifyRequest\"> \n<version>HLR_SUBSCRIBER_v45</version>\n" \
                          "   <objectclass>Subscriber</objectclass>\n <identifier>"+data[0][i]+"</identifier>\n" \
                          " <modification operation=\"addorset\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n " \
                          "  <valueObject xsi:type=\"subscriber:HLR\"> \n <ntype>single</ntype>\n" \
                          " <mobileSubscriberType>genericSubscriber</mobileSubscriberType>\n" \
                          " <umtsSubscriber>\n <accTypeGSM>true</accTypeGSM>\n <accTypeGERAN>true</accTypeGERAN>\n" \
                          " <accTypeUTRAN>true</accTypeUTRAN>\n" \
                          " </umtsSubscriber>\n" \
                          "<wllSubscriber>false</wllSubscriber>\n <mscat>10</mscat>\n" \
                          "<odboc>0</odboc> \n <odbic>0</odbic> \n <odbr>0</odbr>\n <odboprc>1</odboprc> <odbssm>1</odbssm>\n" \
                          "<clip>true</clip>\n <clipOverride>false</clipOverride>\n <clir>3</clir>\n <colpOverride>false</colpOverride>\n" \
                          " <odbgprs>0</odbgprs>\n <sr>1</sr>\n <odbsci>0</odbsci> \n<ts61> \n<msisdn>"+data[1][i]+"</msisdn>\n" \
                          "</ts61> \n <gprs> \n <msisdn>"+data[1][i]+"</msisdn>\n </gprs>\n <obGprs>0</obGprs>\n" \
                           "<optimalRouting>true</optimalRouting>\n <ndcLac>794</ndcLac> \n<pdpContext> \n <id>4</id>\n" \
                           "<type>2</type>\n <qosProfile>PROFIL01</qosProfile> \n <apn>gedaapn</apn> \n <apnArea>HPLMN</apnArea>\n " \
                           "</pdpContext> \n <ocsi> \n<operatorServiceName>OCS5QOAMO</operatorServiceName>\n <csiState>1</csiState>\n" \
                          "<csiNotify>2</csiNotify>\n </ocsi>\n <tcsi>\n <operatorServiceName>OCS5QOAMT</operatorServiceName>\n" \
                           "<csiState>1</csiState> \n<csiNotify>2</csiNotify>\n </tcsi> \n<ucsiserv>OCS5QOAUC</ucsiserv> \n " \
                            "</valueObject> \n </modification> \n </request> \n\n"
        spml_list.append(spml_cnt)
    spml_list.append(foot)
    return (spml_list,rec_count)


def write_to_spml(param_spml,param_count):
    out_file_ext = ".spml"
    resfilename = "data_sim_hlr"
    spmlFile = tempfile.NamedTemporaryFile(delete=False)

    try:
        spmlFile = open(".\\out\\" + resfilename + out_file_ext, "w")
    except:
        print("Error while opening file: "+resfilename+out_file_ext)
        exit()
    spmlFile.write('\t\t\n\n'.join(param_spml))
    spmlFile.close()
    print(param_count.__str__() + " RECORDS GENERATED SUCCESSFULLY: \n\n")


if __name__ == '__main__':
    (spml, count) = generate_spml()
    write_to_spml(spml, count)

   #