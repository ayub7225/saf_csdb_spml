import numpy as np
import tempfile
'#!/usr/bin/env python'
'# -*- encoding: utf-8 -*-'


'#Copyright Ayub Kibirio         '
'#Kenya 2019/08/01                '
'#Start of the Handler Project'
'#!/usr/bin/env python'


Line = "================================================================================\n"
print(Line)
print(">>> AYUB : SPRINTECHLAB | CSDB SPML GENERATOR")
print(Line)

'#function to generate SPML'
'#get data from an input file:'
'#D:\2018-2019\SDM Planning\LTE SHARING\Work Area\ltecards.txt'


def generate_lte_spml():
    input_file = input("Drag $ Drop a file [IMSI,MSISDN,A4KI,ICCID]: ")
    '#define imsi and msisdn variables as arrays:'
    imsi = []
    msisdn = []
    with open(input_file) as file:
        content = file.read()
        my_data = content.splitlines()
        for item in my_data:
            nam_item = item.split(',')
            np_nam_item = np.array(nam_item)
            imsi.append(np_nam_item[0])
            msisdn.append(np_nam_item[1])
            '#convert imsi and msisdn as numpy array'
    data = [imsi,msisdn]

    head = "<spml:batchRequest \n language=\"en_us\" \n execution=\"synchronous\" \n " \
           "processing=\"parallel\" \n xmlns:spml=\"urn:siemens:names:prov:gw:SPML:2:0\" \n " \
           "xmlns:subscriber=\"urn:siemens:names:prov:gw:HSS_SUBSCRIBER:8:2\"  \n " \
           "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"  \n onError=\"resume\"> \n " \
           "<version xmlns:subscriber=\"urn:siemens:names:prov:gw:HSS_SUBSCRIBER:8:2\">HSS_SUBSCRIBER_v82</version>"

    foot = "</spml:batchRequest>"

    spml_list = []
    spml_list.append(head)

    for i in range(len(data[0])):
        rec_count = len(data[0])
        lte_spml = "<request xsi:type=\"spml:ModifyRequest\"> \n " \
                   "<version>HSS_SUBSCRIBER_v82</version> \n " \
                   "<objectclass>Subscriber</objectclass> \n " \
                   "<identifier>"+data[0][i]+"</identifier> \n " \
                   "<modification name=\"hlr/eps\" operation=\"addorset\" scope=\"uniqueTypeMapping\"> \n " \
                   "<valueObject xmlns:ns2=\"urn:siemens:names:prov:gw:HSS_SUBSCRIBER:8:2\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"subscriber:EPS\"> \n " \
                     "<defaultPdnContextId>1</defaultPdnContextId>\n " \
                     "<maxBandwidthUp>250000000</maxBandwidthUp>\n " \
                     "<maxBandwidthDown>250000000</maxBandwidthDown>\n " \
                     "<vplmnIdS6a>36F920</vplmnIdS6a>\n " \
                     "</valueObject>\n " \
                     "</modification>\n " \
                     "<modification name=\"hlr/epsPdnContext\" operation=\"addorset\" scope=\"uniqueTypeMapping\">\n " \
                     "<valueObject xmlns:ns2=\"urn:siemens:names:prov:gw:HSS_SUBSCRIBER:8:2\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"subscriber:EPSPdnContext\">\n " \
                     "<apn>gedaapn</apn>\n " \
                     "<contextId>1</contextId>\n " \
                     "<type>ipv4</type>\n " \
                     "<pdnGwDynamicAllocation>true</pdnGwDynamicAllocation>\n " \
                     "<vplmnAddressAllowed>true</vplmnAddressAllowed>\n " \
                     "<maxBandwidthUp>250000000</maxBandwidthUp>\n " \
                     "<maxBandwidthDown>250000000</maxBandwidthDown>\n " \
                     "<qos>PROFILO1</qos>\n " \
                     "</valueObject>\n " \
                     "</modification>\n " \
                     "</request>\n"

        spml_list.append(lte_spml)
    spml_list.append(foot)
    return (spml_list,rec_count)


def write_to_spml(param_spml,param_count):
    out_file_ext = ".spml"
    resfilename = "data_sim_hss"
    spmlFile = tempfile.NamedTemporaryFile(delete=False)

    try:
        spmlFile = open(".\\out\\" + resfilename + out_file_ext, "w")
    except:
        print("Error while opening file: "+resfilename+out_file_ext)
        exit()
    spmlFile.write('\n\n'.join(param_spml))
    spmlFile.close()
    print(param_count.__str__() + " RECORDS GENERATED SUCCESSFULLY: \n\n")


if __name__ == '__main__':
    (spml, count) = generate_lte_spml()
    write_to_spml(spml, count)

   #